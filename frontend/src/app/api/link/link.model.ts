export interface LinkModel {

    originalUrl: string;

    hash: string;
    shortUrl: string;

    createdAt: string;
    lastAccessAt: string;

    stats?: {
        totalClicks: number;
        os: { [key: string]: number };
        browser: { [key: string]: number };
    };
}
