import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { map } from 'rxjs/operators';

@Injectable()
export class ApiResponseInterceptor implements NestInterceptor<any, any> {
    intercept(context: ExecutionContext, next: CallHandler<any>) {
        return next.handle().pipe(
            map(response => {
                // if response is "undefined", we explicitly want it to be "null",
                // so "data" property doesn't get removed
                return {
                    data: response || null
                };
            })
        );
    }
}
