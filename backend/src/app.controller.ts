import { Controller, Get, Param, Res, Req } from '@nestjs/common';
import { LinkService } from './api/link/link.service';

@Controller()
export class AppController {

    constructor(private urlService: LinkService) {

    }

    @Get(':hash')
    async performRedirect(@Param('hash') hash: string, @Req() req, @Res() res) {

        // Please Note: because we have injected @Res we can't rely on Nestjs
        // interceptors anymore and therefore we must use
        // built-in express functions of res object in order to build response

        // if hash ends with + we should redirect user to stats page
        if (hash.endsWith('+')) {
            const hashWithoutPlus = hash.substr(0, hash.length - 1);
            return res.redirect(process.env.FRONTEND_URL + '/stats/' + hashWithoutPlus);
        }

        // we need to replace . with _ so mongodb doesn't try to create "keys"
        // e.g. (OS = Windows 10.0)
        const os = String(req.useragent.os).replace('.', '_');
        const browser = String(req.useragent.browser).replace('.', '_');

        const statIncrements = {};
        statIncrements[`stats.totalClicks`] = 1;
        statIncrements[`stats.browser.${browser}`] = 1;
        statIncrements[`stats.os.${os}`] = 1;

        // fetch link by code
        const link = await this.urlService.findOneAndUpdate({ hash }, {
            $inc: statIncrements,
            $set: { lastAccessAt: new Date() }
        });

        // in case link not found
        if (!link) {
            return res.status(404).end('Invalid link / code provided.');
        }

        // perform actual redirect
        return res.redirect(link.originalUrl);
    }
}
