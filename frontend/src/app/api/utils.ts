import { ApiResponse } from './api-reponse.model';


export function getFirstValidationError(response: ApiResponse<any>): string | undefined {

    return undefined;
}

export function extractErrorMessage(response: ApiResponse<any>): string | undefined {
    if (response.error.type === 'VALIDATION') {
        if (Array.isArray(response.error.details)) {
            for (const err of response.error.details) {
                if (err.constraints) {
                    const msgs = Object.values(err.constraints) as any[];
                    if (msgs.length > 0) {
                        return msgs[0];
                    }
                }
            }
        }
    } else {
        return response.error.type + ' — ' + response.error.details;
    }
}
