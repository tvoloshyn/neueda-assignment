import { Exclude, Expose } from 'class-transformer';
import { Column, Entity, Index, ObjectIdColumn, UpdateDateColumn, CreateDateColumn } from 'typeorm';
import { StatsInfo } from './stat.entity';

@Entity('links')
export class LinkEntity {

    @Exclude({ toPlainOnly: true })
    @ObjectIdColumn()
    id: string;

    @Index()
    @Column()
    originalUrl: string;

    @Column({ unique: true })
    hash: string;

    @Column()
    isCustom: boolean;

    @Expose()
    get shortUrl() {
        return process.env.REDIRECT_URL + '/' + this.hash;
    }

    @Column(() => StatsInfo)
    stats: StatsInfo;

    @Column()
    createdAt: Date;

    @Column()
    lastAccessAt: Date;
}
