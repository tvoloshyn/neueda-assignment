import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, ComponentFixture, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ApiLinkService } from '../api/link/link.service';
import { By } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { StatsPageComponent } from './stats-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { Subject, of } from 'rxjs';

describe('StatsPageComponent', () => {

    let service: ApiLinkService;
    let httpMock: HttpTestingController;
    let fixture: ComponentFixture<StatsPageComponent>;
    let params = new Subject();

    const LINK_ID = 'DnMdlsZ';

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [
                StatsPageComponent
            ],
            providers: [
                ApiLinkService,
                {
                    provide: ActivatedRoute,
                    useValue: { params: of({ id: LINK_ID }) }
                }
            ],
            imports: [
                HttpClientTestingModule,
                RouterTestingModule,
                AlertModule.forRoot()
            ],
        }).compileComponents();

        params = new Subject();
        service = TestBed.get(ApiLinkService);
        httpMock = TestBed.get(HttpTestingController);
        fixture = TestBed.createComponent(StatsPageComponent);
    });

    it('should create component', () => {
        expect(fixture.componentInstance).toBeDefined();
    });

    it('should display error when link is not found', () => {

        fixture.detectChanges();

        // generate API response
        httpMock.expectOne(environment.baseApiUrl + `/api/v1/links/${LINK_ID}`).flush({
            error: {
                code: 400,
                type: 'Not Found',
                details: 'Link was not found'
            }
        });

        const alert = fixture.debugElement.query(By.css('alert'));

        expect(fixture.componentInstance.errorMsg).toBeDefined();
        expect(alert).toBeDefined();
    });

    it('should display stats', () => {

        fixture.detectChanges();

        const DATE = '2019-10-17T18:12:12.391Z';

        // generate API response
        httpMock.expectOne(environment.baseApiUrl + `/api/v1/links/${LINK_ID}`).flush({
            data: {
                originalUrl: 'http://google.com',
                hash: LINK_ID,
                isCustom: false,
                createdAt: DATE,
                stats: {
                    totalClicks: 1,
                    browser: {
                        Chrome: 1
                    },
                    os: {
                        'Windows 10_0': 1
                    }
                }
            }
        });

        fixture.detectChanges();

        const tds = fixture.debugElement.queryAll(By.css('.stats-table>tbody>tr>td'));

        expect(tds.length).toEqual(4);
        expect(tds[0].nativeElement.textContent).toEqual('17-Oct-2019 19:12');
        expect(tds[1].nativeElement.textContent).toEqual('—');
        expect(tds[2].nativeElement.textContent).toEqual('http://google.com');
        expect(tds[3].nativeElement.textContent).toEqual('1');
    });

    afterEach(() => {
        httpMock.verify();
    });
});
