import { HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

/**
 * This is a helper function which helps catching API related error and return them
 * with subscribe result callback intead of error callback
 */
export function handleResponse<T>() {
    return (source: Observable<T>) => {
        return source.pipe(catchError(error => {
            // determine if error is API Response Error
            if (error instanceof HttpErrorResponse) {
                if (error.error) {
                    const rawResponse = error.error;
                    if (rawResponse.error && rawResponse.error.code && rawResponse.error.type) {
                        return of<T>(rawResponse);
                    }
                }
                // if it is not, then re-throw
                return throwError(error);
            }
        }));
    };
}
