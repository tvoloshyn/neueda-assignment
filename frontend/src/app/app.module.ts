import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertModule } from 'ngx-bootstrap/alert';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';
import { MainPageModule } from './main-page/main-page.module';
import { StatsPageModule } from './stats-page/stats-page.module';

@NgModule({

    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        MainPageModule,
        StatsPageModule,
        AlertModule.forRoot()
    ],
    declarations: [
        AppComponent
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
