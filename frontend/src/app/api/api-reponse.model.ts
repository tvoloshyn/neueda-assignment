export interface ApiResponse<T> {
    error?: {
        code: number;
        type: string;
        details?: any;
    };
    data?: T;
}
