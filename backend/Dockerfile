### STAGE 1: Build ###

FROM node:10-alpine as builder

COPY package.json package-lock.json ./

# Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm ci && mkdir /node-app && mv ./node_modules ./node-app

WORKDIR /node-app

# add `/node-app/node_modules/.bin` to $PATH
ENV PATH /node-app/node_modules/.bin:$PATH

COPY . .

RUN npm run build

### STAGE 2: Setup ###
FROM node:10-alpine

WORKDIR /node-app

COPY --from=builder /node-app/dist .
COPY package.json package-lock.json ./

RUN npm install --production

CMD ["node", "main"]