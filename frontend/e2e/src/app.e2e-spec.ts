import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('URL Shortener', () => {
    let page: AppPage;

    beforeEach(() => {
        page = new AppPage();
    });

    it('should convert long url to short url', async () => {
        page.navigateTo();

        // input value
        await page.getUrlInput().sendKeys('https://google.com');

        const btn = page.getShortenButton();

        await btn.click();

        // wait for request to complete
        await browser.wait(browser.ExpectedConditions.elementToBeClickable(btn));

        // retrieve
        const value = await page.getUrlInput().getAttribute('value');

        // if url has changed then we have recieved a response back with shortened URL
        expect(value).not.toEqual('https://google.com');
    });

    it('should add shortened url to history', async () => {
        page.navigateTo();

        const TEST_URL = 'https://test-url.com';

        // input value
        await page.getUrlInput().sendKeys(TEST_URL);

        const btn = page.getShortenButton();

        await btn.click();

        // wait for request to complete
        await browser.wait(browser.ExpectedConditions.elementToBeClickable(btn));

        // retrieve
        const items = page.getHistoryList();

        // see if first item contains URL we just converted
        expect(items.first().getText()).toContain(TEST_URL);
    });

    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });
});
