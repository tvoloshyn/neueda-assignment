// tslint:disable-next-line: no-var-requires
require('dotenv').config();

import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as useragent from 'express-useragent';
import { ApiExceptionsFilter } from './api/api-exception.filter';
import { ApiResponseInterceptor } from './api/api-response.interceptor';
import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.enableCors();
    app.useGlobalInterceptors(new ApiResponseInterceptor());
    app.useGlobalFilters(new ApiExceptionsFilter());
    app.useGlobalPipes(new ValidationPipe());
    app.use(useragent.express());
    await app.listen(3000);
}
bootstrap();
