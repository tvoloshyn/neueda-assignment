import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LinkModule } from './api/link/link.module';
import { AppController } from './app.controller';

@Module({
    imports: [
        // need to use async module resolution in case testing
        TypeOrmModule.forRootAsync({
            useFactory: async () => {
                return {
                    type: 'mongodb',
                    host: process.env.DATABASE_HOST,
                    port: +process.env.DATABASE_PORT,
                    database: process.env.DATABASE_NAME,
                    entities: [__dirname + '/**/*.entity{.ts,.js}'],
                    synchronize: true,
                    useNewUrlParser: true,
                    useUnifiedTopology: true
                } as any;
            }
        }),
        LinkModule
    ],
    controllers: [
        AppController
    ],
    providers: [

    ]
})
export class AppModule { }
