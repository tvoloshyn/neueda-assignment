import { IsOptional, IsDefined, IsString, IsUrl, IsNotEmpty, MinLength, IsAlphanumeric } from 'class-validator';

export class CreateLinkDto {

    @IsDefined()
    @IsString()
    @IsUrl({ require_valid_protocol: true }, {
        message: 'Please provide valid URL'
    })
    url: string;

    @IsOptional()
    @IsNotEmpty()
    @MinLength(2)
    @IsAlphanumeric()
    customName: string;
}
