import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page.component';
import { ShortLinkInputModule } from '../short-link-input/short-link-input.module';
import { ShortLinkHistoryListModule } from '../short-link-history-list/short-link-history-list.module';

@NgModule({
    imports: [
        CommonModule,
        ShortLinkInputModule,
        ShortLinkHistoryListModule
    ],
    declarations: [MainPageComponent],
    exports: [MainPageComponent]
})
export class MainPageModule { }
