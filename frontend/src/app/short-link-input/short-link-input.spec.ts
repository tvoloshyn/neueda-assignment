import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, ComponentFixture, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ApiLinkService } from '../api/link/link.service';
import { ShortLinkInputComponent } from './short-link-input.component';
import { By } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

describe('ShortLinkInputComponent', () => {

    let service: ApiLinkService;
    let httpMock: HttpTestingController;
    let fixture: ComponentFixture<ShortLinkInputComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [
                ShortLinkInputComponent
            ],
            providers: [
                ApiLinkService
            ],
            imports: [
                HttpClientTestingModule,
                FormsModule,
                AlertModule.forRoot()
            ],
        }).compileComponents();

        service = TestBed.get(ApiLinkService);
        httpMock = TestBed.get(HttpTestingController);
        fixture = TestBed.createComponent(ShortLinkInputComponent);
    });

    it('should create component', () => {
        expect(fixture.componentInstance).toBeDefined();
    });

    it('should display error when no URL is provided', () => {
        const btn = fixture.debugElement.query(By.css('button'));
        const alert = fixture.debugElement.query(By.css('alert'));

        btn.triggerEventHandler('click', null);

        expect(fixture.componentInstance.errorMsg).toBeDefined();
        expect(alert).toBeDefined();
    });

    it('should display error when invalid URL is provided', () => {

        const btn = fixture.debugElement.query(By.css('button'));
        const alert = fixture.debugElement.query(By.css('alert'));

        // set input URL
        fixture.componentInstance.urlInput = 'hppt://test';
        fixture.detectChanges();

        // trigger click
        btn.triggerEventHandler('click', null);

        // generate API response
        httpMock.expectOne(environment.baseApiUrl + '/api/v1/links').flush({
            error: {
                code: 400,
                type: 'VALIDATION',
                details: [{
                    constraints: {
                        isUrl: 'Please provide valid URL'
                    }
                }]
            }
        });

        expect(fixture.componentInstance.errorMsg).toBeDefined();
        expect(alert).toBeDefined();
    });

    it('should display shortened link within input', async () => {

        const EXPECTED_URL = 'http://localhost:3000/FsdZxkw';
        const btn = fixture.debugElement.query(By.css('button'));
        const input = fixture.debugElement.query(By.css('input'));

        // set input URL
        fixture.componentInstance.urlInput = 'http://google.com';
        fixture.detectChanges();

        // trigger click
        btn.triggerEventHandler('click', null);

        // generate API response
        httpMock.expectOne(environment.baseApiUrl + '/api/v1/links').flush({
            data: {
                shortUrl: EXPECTED_URL
            }
        });
        fixture.detectChanges();
        await fixture.whenStable();

        expect(input.nativeElement.value).toEqual(EXPECTED_URL);
    });

    it('should display custom name input when user prompts for it', () => {

        const btn = fixture.debugElement.query(By.css('u.cursor-pointer'));

        btn.triggerEventHandler('click', null);
        fixture.detectChanges();

        const inputGroupEl = fixture.debugElement.query(By.css('.input-group'));

        expect(inputGroupEl).toBeDefined();
    });

    it('should add http:// to URL if user has not specified protocol', () => {

        const btn = fixture.debugElement.query(By.css('button'));

        // set input URL
        fixture.componentInstance.urlInput = 'google.com';
        fixture.detectChanges();

        // trigger click
        btn.triggerEventHandler('click', null);

        // generate API response
        const req = httpMock.expectOne(environment.baseApiUrl + '/api/v1/links');
        expect(req.request.body.url).toEqual('http://google.com');

        req.flush({ data: {} });
    });

    afterEach(() => {
        httpMock.verify();
    });
});
