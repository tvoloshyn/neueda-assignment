import { ShortLinkHistoryService } from './short-link-history.service';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ShortLinkHistoryListComponent } from './short-link-history-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

describe('ShortLinkHistoryListComponent', () => {

    let service: ShortLinkHistoryService;
    let fixture: ComponentFixture<ShortLinkHistoryListComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [
                ShortLinkHistoryListComponent
            ],
            providers: [
                ShortLinkHistoryService
            ],
            imports: [
                RouterTestingModule
            ],
        }).compileComponents();

        service = TestBed.get(ShortLinkHistoryService);
        fixture = TestBed.createComponent(ShortLinkHistoryListComponent);

        localStorage.removeItem(service.HISTORY_KEY);
    });

    it('should create component', () => {
        expect(fixture.componentInstance).toBeDefined();
    });

    it('should display link within the history list', () => {

        const HASH = 'gHlmQpdS';
        const ORIGINAL_URL = 'http://google.com';

        const LINK = {
            hash: HASH,
            originalUrl: ORIGINAL_URL
        };

        service.init();
        service.saveLink(LINK);
        fixture.detectChanges();

        const span = fixture.debugElement.query(By.css('.original-link-text'));

        expect(span.nativeElement.textContent).toContain(ORIGINAL_URL);
        expect(fixture.componentInstance.links).toContain(LINK);
    });
});
