import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShortLinkHistoryListComponent } from './short-link-history-list.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations: [ShortLinkHistoryListComponent],
    exports: [ShortLinkHistoryListComponent]
})
export class ShortLinkHistoryListModule { }
