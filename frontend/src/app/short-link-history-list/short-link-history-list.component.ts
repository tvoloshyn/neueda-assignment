import { Component, OnInit, OnDestroy } from '@angular/core';
import { LinkModel } from '../api/link/link.model';
import { ShortLinkHistoryService } from './short-link-history.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-short-link-history-list',
    templateUrl: './short-link-history-list.component.html',
    styleUrls: ['./short-link-history-list.component.scss']
})
export class ShortLinkHistoryListComponent implements OnInit, OnDestroy {

    links: Partial<LinkModel>[] = [];

    private onDestroy$ = new Subject();

    constructor(private linkHistoryService: ShortLinkHistoryService) { }

    ngOnInit() {
        this.linkHistoryService.links$.pipe(takeUntil(this.onDestroy$)).subscribe(links => {
            this.links = links;
        });
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }
}
