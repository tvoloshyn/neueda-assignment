import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShortLinkInputComponent } from './short-link-input.component';
import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AlertModule
    ],
    declarations: [ShortLinkInputComponent],
    exports: [ShortLinkInputComponent]
})
export class ShortLinkInputModule { }
