import { browser, by, element } from 'protractor';

export class AppPage {
    navigateTo() {
        return browser.get(browser.baseUrl);
    }

    getUrlInput() {
        return element(by.css('app-short-link-input input'));
    }

    getShortenButton() {
        return element(by.css('app-short-link-input button'));
    }

    getHistoryList() {
        return element.all(by.css('app-short-link-history-list .list-group-item'));
    }
}
