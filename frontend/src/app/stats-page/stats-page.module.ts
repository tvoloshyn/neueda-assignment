import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsPageComponent } from './stats-page.component';
import { AlertModule } from 'ngx-bootstrap/alert';

@NgModule({
    imports: [
        CommonModule,
        AlertModule
    ],
    declarations: [StatsPageComponent]
})
export class StatsPageModule { }
