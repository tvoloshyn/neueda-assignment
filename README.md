# Introduction

This project is built using the following tech. stack:
- NestJS for `backend` (see https://nestjs.com)
- Angular 8 for `frontend` (see https://angular.io)
- MongoDB as `database` (see https://www.mongodb.com)

In order to run this project, install `Docker` and `docker-compose` on your system and issue following command in the root of this project:
```
docker-compose up --build
```

This command will do the following:
- dockerize NestJs (node) application (port `3000`)
- perform multi-stage build on frontend compiling angular application and serving it through nginx (port `4200`)
- create mongodb database with no volumes (for the purpose of the demo)

Once building is done, you should see in your console an output which states the following:
```
[NestApplication] Nest application successfully started
```

You can then navigate to http://localhost:4200 to see the frontend application.

# Backend API

The backend API consist of 2 main endpoints:
- `POST /api/v1/links` which takes a JSON object with the following properties:
    - `url` — URL to be shortened
    - `customName` — (optional) custom name for shortened URL which will be used instead of generated hash
- `GET /api/v1/links/{hash}`
where `hash` is unique "short" id of a URL.

# Testing
- Backend API tests are located under `./backend/test` and are performed with the help of `mongodb-memory-server`
- Frontend `unit` tests are located within each of the component folders
- Frontend `e2e` tests are located under `./frontend/e2e`

# Implemented improvements

1. Perform check whether the URL submitted by user already exists in the database and if so, return it (with its short code). This helps to save some space in DB, in case many people are sharing link to something "viral". 

2. Allow user to specify custom (short) name for URL.

3. Store history of last 5 shortened links in local storage and display them to user

4. Adding + sign at the end of shortened link will redirect user to statistics page (e.g. http://localhost:3000/1234567+)

5. If user does not specify valid protocol, default it to `http://`

