import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { QueryFailedError } from 'typeorm';
import { ValidationError } from 'class-validator';

@Catch()
export class ApiExceptionsFilter implements ExceptionFilter {

    constructor(private consoleLogError = true) {

    }

    catch(exception: any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        if (this.consoleLogError) {
            // tslint:disable-next-line:no-console
            console.error(exception);
        }

        let statusCode = 500;
        let type = 'SERVER';
        let details;

        if (exception instanceof HttpException) {

            statusCode = exception.getStatus();
            const error = exception.message;

            if (error.error) {
                type = error.error;
            }
            if (error.message) {
                details = error.message;
            }

            // check if this is class validator error
            if (Array.isArray(details) && details.length > 0) {
                if (details[0] instanceof ValidationError) {
                    type = 'VALIDATION';
                }
            }
        } else if (exception instanceof QueryFailedError) {
            type = 'DB_ERROR';
            const dbErr = exception as any;
            details = {
                code: dbErr.code,
                message: String(dbErr.detail || dbErr.message)
            };
        } else {
            details = String(exception);
        }

        response.status(statusCode).json({
            error: {
                code: statusCode,
                type,
                details
            }
        });
    }
}
