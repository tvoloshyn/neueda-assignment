import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ApiLinkService } from '../api/link/link.service';
import { extractErrorMessage } from '../api/utils';
import { ShortLinkHistoryService } from '../short-link-history-list/short-link-history.service';

@Component({
    selector: 'app-short-link-input',
    templateUrl: './short-link-input.component.html',
    styleUrls: ['./short-link-input.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShortLinkInputComponent implements OnInit {

    isLoading = false;
    urlInput: string;
    errorMsg: string;
    showCustomName = false;
    customNameInput: string;

    constructor(private linkApi: ApiLinkService,
                private cdr: ChangeDetectorRef,
                private linkHistoryService: ShortLinkHistoryService) { }

    ngOnInit() {
    }

    getShortDomain() {
        return environment.baseShortUrl;
    }

    shortenUrl() {
        if (!this.urlInput) {
            this.errorMsg = 'URL can not be empty';
            this.cdr.markForCheck();
        } else {

            // perform check whether use has specificed the protocol
            // and if not, use http by default
            if (!this.urlInput.startsWith('http')) {
                this.urlInput = `http://${this.urlInput}`;
            }

            this.isLoading = true;
            this.cdr.markForCheck();

            this.linkApi.create({
                url: this.urlInput,
                customName: this.customNameInput
            }).pipe(take(1)).subscribe(response => {
                if (response.error) {
                    this.errorMsg = extractErrorMessage(response);
                } else {
                    this.urlInput = response.data.shortUrl;
                    this.linkHistoryService.saveLink(response.data);

                    // "reset" values
                    this.customNameInput = undefined;
                    this.showCustomName = false;
                }
                this.isLoading = false;
                this.cdr.markForCheck();
            });
        }
    }

}
