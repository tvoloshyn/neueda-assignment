import { Module } from '@nestjs/common';
import { UrlController } from './link.controller';
import { LinkService } from './link.service';

@Module({
    controllers: [
        UrlController
    ],
    providers: [
        LinkService
    ],
    exports: [LinkService]
})
export class LinkModule {

}
