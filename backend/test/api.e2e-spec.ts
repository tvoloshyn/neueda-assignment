import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { NestApplication } from '@nestjs/core';
import { Connection } from 'typeorm';
import * as useragent from 'express-useragent';
import { ValidationPipe } from '@nestjs/common';
import { ApiExceptionsFilter } from '../src/api/api-exception.filter';
import { ApiResponseInterceptor } from '../src/api/api-response.interceptor';

describe('API', () => {
    let app: NestApplication;
    let mongod: MongoMemoryServer;
    let connection: Connection;

    beforeEach(async () => {
        mongod = new MongoMemoryServer();

        // set env vars
        process.env.NODE_ENV = 'test';
        process.env.FRONTEND_URL = 'http://localhost:4200';
        process.env.REDIRECT_URL = 'http://localhost:3000';

        process.env.DATABASE_URL = await mongod.getConnectionString();
        process.env.DATABASE_PORT = String(await mongod.getPort());
        process.env.DATABASE_NAME = await mongod.getDbName();

        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        app.useGlobalInterceptors(new ApiResponseInterceptor());
        app.useGlobalFilters(new ApiExceptionsFilter(false));
        app.useGlobalPipes(new ValidationPipe());
        app.use(useragent.express());

        connection = moduleFixture.get(Connection);

        await app.init();
    });

    afterEach(async () => {
        await connection.close();
        await mongod.stop();
    });

    it('should return 404 when invalid hash is provided', async () => {
        return request(app.getHttpServer())
            .get(`/api/v1/links/dfFdeR`)
            .expect(404);
    });

    it('should create shortened link', () => {
        return request(app.getHttpServer())
            .post('/api/v1/links')
            .send({
                url: 'http://google.com'
            })
            .expect(201)
            .then(response => {
                expect(response.body.data).toBeTruthy();
            });
    });

    it('should return link', async () => {

        const createResp = await request(app.getHttpServer())
            .post('/api/v1/links')
            .send({
                url: 'http://google.com'
            })
            .expect(201);

        return request(app.getHttpServer())
            .get(`/api/v1/links/${createResp.body.data.hash}`)
            .expect(200)
            .then(response => {
                expect(response.body.data.hash).toEqual(createResp.body.data.hash);
            });
    });

    it('should perform redirect', async () => {

        const createResp = await request(app.getHttpServer())
            .post('/api/v1/links')
            .send({
                url: 'http://google.com'
            })
            .expect(201);

        return request(app.getHttpServer())
            .get(`/${createResp.body.data.hash}`)
            .expect(302);
    });

    afterAll(async () => {
        await app.close();
    });
});
