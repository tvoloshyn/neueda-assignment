import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LinkModel } from './link.model';
import { ApiResponse } from '../api-reponse.model';
import { handleResponse } from '../api-response.handler';

@Injectable({
    providedIn: 'root'
})
export class ApiLinkService {

    constructor(private http: HttpClient) {

    }

    create(values: { url: string; customName?: string }) {
        const URL = environment.baseApiUrl + '/api/v1/links';
        const body = { ...values };
        return this.http.post<ApiResponse<LinkModel>>(URL, body).pipe(
            handleResponse()
        );
    }

    read(hash: string) {
        const URL = environment.baseApiUrl + `/api/v1/links/${hash}`;
        return this.http.get<ApiResponse<LinkModel>>(URL).pipe(
            handleResponse()
        );
    }
}
