import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import * as generate from 'nanoid/generate';
import { Connection, FindOneOptions } from 'typeorm';
import { CreateLinkDto } from './dto/create-link.dto';
import { LinkEntity } from './entity/link.entity';

@Injectable()
export class LinkService {

    readonly DICTIONARY = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    readonly HASH_LENGTH = 7;

    constructor(@InjectConnection() private connection: Connection) {

    }

    async create(dto: CreateLinkDto) {
        const rep = this.connection.getMongoRepository(LinkEntity);

        const entity = rep.create({
            originalUrl: dto.url,
            createdAt: new Date()
        });

        // if custom name was provided, use it
        if (dto.customName) {
            entity.hash = dto.customName;
            entity.isCustom = true;
        } else {
            // otherwise generate id
            entity.hash = generate(this.DICTIONARY, this.HASH_LENGTH);
            entity.isCustom = false;
        }

        return await rep.save(entity);
    }

    async findOne(conditions: FindOneOptions<LinkEntity>) {
        return await this.connection.getMongoRepository(LinkEntity).findOne(conditions);
    }

    async findOneAndUpdate(query: any, conditions: any) {
        const result = await this.connection.getMongoRepository(LinkEntity).findOneAndUpdate(query, conditions);
        return result.value as LinkEntity;
    }
}
