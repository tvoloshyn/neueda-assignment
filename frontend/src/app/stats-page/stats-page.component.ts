import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewChecked, AfterViewInit } from '@angular/core';
import { ApiLinkService } from '../api/link/link.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, ReplaySubject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { LinkModel } from '../api/link/link.model';
import * as Chart from 'chart.js';
import { extractErrorMessage } from '../api/utils';

@Component({
    selector: 'app-stats-page',
    templateUrl: './stats-page.component.html',
    styleUrls: ['./stats-page.component.scss']
})
export class StatsPageComponent implements OnInit, OnDestroy, AfterViewInit {

    errorMsg: string;

    @ViewChild('osChart', { static: false }) osChartElRef: ElementRef<any>;
    @ViewChild('browserChart', { static: false }) browserChartElRef: ElementRef<any>;

    private onDestroy$ = new Subject();

    private osChart: Chart;
    private browserChart: Chart;

    private osChartData$ = new ReplaySubject<LinkModel>(1);
    private browserChartData$ = new ReplaySubject<LinkModel>(1);

    link: LinkModel;

    constructor(
        private linkApi: ApiLinkService,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.route.params.pipe(takeUntil(this.onDestroy$)).subscribe(params => {
            if (params.id) {
                this.loadLinkData(params.id);
            } else {
                this.router.navigateByUrl('/');
            }
        });
    }

    loadLinkData(id: string) {
        this.linkApi.read(id).pipe(take(1)).subscribe(response => {
            if (response.error) {
               this.errorMsg = extractErrorMessage(response);
            } else {
                this.link = response.data;
                this.osChartData$.next(this.link);
                this.browserChartData$.next(this.link);
            }
        });
    }

    ngAfterViewInit() {
        this.osChart = new Chart(this.osChartElRef.nativeElement, {
            type: 'horizontalBar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                    }
                },
                responsive: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    yAxes: [{
                        maxBarThickness: 50,
                    }]
                }
            }
        });

        this.browserChart = new Chart(this.browserChartElRef.nativeElement, {
            type: 'horizontalBar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                    }
                },
                responsive: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        barPercentage: 0.4,
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    yAxes: [{
                        maxBarThickness: 50,
                    }]
                }
            }
        });

        this.osChartData$.subscribe(link => {
            if (link.stats && link.stats.os) {
                this.osChart.data.labels = [];
                this.osChart.data.datasets = [{
                    data: [],
                    backgroundColor: '#4285f4'
                }];
                for (const [key, value] of Object.entries(link.stats.os)) {
                    this.osChart.data.labels.push(key);
                    this.osChart.data.datasets[0].data.push(value);
                }
                this.osChart.update();
            }
        });

        this.browserChartData$.subscribe(link => {
            if (link.stats && link.stats.browser) {
                this.browserChart.data.labels = [];
                this.browserChart.data.datasets = [{
                    data: [],
                    backgroundColor: '#4285f4'
                }];
                for (const [key, value] of Object.entries(link.stats.browser)) {
                    this.browserChart.data.labels.push(key);
                    this.browserChart.data.datasets[0].data.push(value);
                }
                this.browserChart.update();
            }
        });
    }

    ngOnDestroy() {
        this.onDestroy$.next();
        this.onDestroy$.complete();

        if (this.osChart) {
            this.osChart.destroy();
        }
        if (this.browserChart) {
            this.browserChart.destroy();
        }
    }
}
