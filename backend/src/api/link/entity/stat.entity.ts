import { Column } from 'typeorm';

export class StatsInfo {

    @Column()
    totalClicks: number;

    @Column()
    os: { [key: string]: number };

    @Column()
    browser: { [key: string]: number };
}
