import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { ApiLinkService } from './link.service';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

describe('ApiLinkService', () => {

    let service: ApiLinkService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ApiLinkService
            ],
            imports: [
                HttpClientTestingModule
            ],
        });

        service = TestBed.get(ApiLinkService);
        httpMock = TestBed.get(HttpTestingController);
    });

    it('should create service', () => {
        expect(service).toBeTruthy();
    });

    it('should make request to create link', () => {
        service.create({
            url: 'http://google.com',
            customName: 'google'
        }).pipe(take(1)).subscribe(response => {
            expect(response.data).toBeTruthy();
        });

        const req = httpMock.expectOne(environment.baseApiUrl + '/api/v1/links');

        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toEqual({
            url: 'http://google.com',
            customName: 'google'
        });

        req.flush({ data: {} });
    });

    it('should make request to retrieve link', () => {

        const shortLinkId = 'yJvDfr6';

        service.read(shortLinkId).pipe(take(1)).subscribe(response => {
            expect(response.data).toBeTruthy();
        });

        const req = httpMock.expectOne(environment.baseApiUrl + `/api/v1/links/${shortLinkId}`);

        expect(req.request.method).toEqual('GET');

        req.flush({ data: {} });
    });

    it('should make request and handle error within subscribe callback', () => {
        const shortLinkId = 'yJvDfr6';
        const apiError = { code: 404, type: 'Not Found' };

        service.read(shortLinkId).pipe(take(1)).subscribe(response => {
            expect(response.error).toEqual(apiError);
        });

        const req = httpMock.expectOne(environment.baseApiUrl + `/api/v1/links/${shortLinkId}`);

        req.flush({ error: apiError });
    });

    afterEach(() => {
        httpMock.verify();
    });
});

