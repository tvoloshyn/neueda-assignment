import { BadRequestException, Body, Controller, Get, NotFoundException, Param, Post } from '@nestjs/common';
import { classToPlain } from 'class-transformer';
import { Validator } from 'class-validator';
import { CreateLinkDto } from './dto/create-link.dto';
import { LinkService } from './link.service';

@Controller('/api/v1/links')
export class UrlController {

    constructor(private urlService: LinkService) {

    }

    @Post()
    async createShortLink(@Body() dto: CreateLinkDto) {

        if (dto.customName) {
            // check if custom name already exists (or conflicts with hash)
            const customLink = await this.urlService.findOne({
                where: {
                    hash: dto.customName
                }
            });

            if (customLink) {
                throw new BadRequestException(`Provided custom name '${dto.customName}' is already in use.`);
            }
        } else {
            // check if original URL already exists in database
            // and return in case it does
            const link = await this.urlService.findOne({
                where: {
                    originalUrl: dto.url,
                    isCustom: false
                }
            });

            if (link) {
                return classToPlain(link);
            }
        }

        // otherwise proceed with creation
        const newLink = await this.urlService.create(dto);

        return classToPlain(newLink);
    }

    @Get(':hash')
    async getLink(@Param('hash') hash) {

        const link = await this.urlService.findOne({
            where: { hash }
        });

        if (!link) {
            throw new NotFoundException(`Link with hash "${hash}" does not exist`);
        }

        return classToPlain(link);
    }
}
