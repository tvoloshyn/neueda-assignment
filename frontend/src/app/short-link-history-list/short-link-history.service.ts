import { Injectable } from '@angular/core';
import { LinkModel } from '../api/link/link.model';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ShortLinkHistoryService {

    readonly HISTORY_KEY = 'link-history';
    readonly MAX_LINKS = 5;

    private linksSubj = new BehaviorSubject([]);
    get links$() {
        return this.linksSubj.asObservable();
    }

    init() {
        let links: Partial<LinkModel>[];
        const rawValue = localStorage.getItem(this.HISTORY_KEY);
        if (!rawValue) {
            links = [];
        } else {
            try {
                links = JSON.parse(rawValue);
            } catch (e) {
                console.error(e);
                links = [];
            }
        }
        this.linksSubj.next(links);
    }

    saveLink(link: Partial<LinkModel>) {
        // read current value of links
        const links = this.linksSubj.value;
        links.unshift(link);
        if (links.length > this.MAX_LINKS) {
            links.pop();
        }
        localStorage.setItem(this.HISTORY_KEY, JSON.stringify(links));
        // "notify" everyone about history update
        this.linksSubj.next(links);
        return links;
    }
}
